package bobjia;

import java.util.List;

/**
 * Hello world!
 */
public class App {

    public static void main(String[] args) {
        for (MathTypeEnum e : MathTypeEnum.values()) {
            MathGenerator mathGenerator = new MathGenerator(20, e);
            List<String> result = mathGenerator.generator(90);

            result.forEach(str -> {
                System.out.println(str + "=");
            });
        }

    }
}
