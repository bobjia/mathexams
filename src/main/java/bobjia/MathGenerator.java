package bobjia;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MathGenerator {
    private final int range;
    private final MathTypeEnum type;

    public MathGenerator(final int range, final MathTypeEnum type) {
        this.range = range;
        this.type = type;
    }

    public List<String> generator(final int num) {
        final List<String> result = new ArrayList();
        for (int i = 0; i < num; i++) {
            result.add(generateOne());
        }
        return result;
    }

    public String generateOne() {
        Random rand = new Random();
        int one = rand.nextInt(range);
        int two = rand.nextInt(range);
        int three = rand.nextInt(range);

        if (MathTypeEnum.TWO_PARAMS_ADD == this.type) {
            while (one + two > 20 || one == 0 || two == 0) {
                one = rand.nextInt(range);
                two = rand.nextInt(range);
            }
            return one + "+" + two;
        } else if (MathTypeEnum.TWO_PARAMS_MINUS == this.type) {
            while (one == two || one == 0 || two == 0) {
                one = rand.nextInt(range);
                two = rand.nextInt(range);
            }
            if (one < two) {
                return two + "-" + one;
            } else {
                return one + "-" + two;
            }
        } else if (MathTypeEnum.THREE_PARAMS_ADD == this.type) {
            while (one + two + three > 20 || one == 0 || two == 0 || three == 0) {
                one = rand.nextInt(range);
                two = rand.nextInt(range);
                three = rand.nextInt(range);
            }
            return one + "+" + two + "+" + three;

        } else if (MathTypeEnum.THREE_PARAMS_MINUS == this.type) {
            while (one <= (two + three) || one == 0 || two == 0 || three == 0) {
                one = rand.nextInt(range);
                two = rand.nextInt(range);
                three = rand.nextInt(range);
            }
            return one + "-" + two + "-" + three;
        } else if (MathTypeEnum.THREE_PARAMS_ADD_MINUS == this.type) {
            while ((one + two) <= three || one == 0 || two == 0 || three == 0||two>10||three>10) {
                one = rand.nextInt(range);
                two = rand.nextInt(10);
                three = rand.nextInt(10);
            }
            return one + "+" + two + "-" + three;
        } else if (MathTypeEnum.THREE_PARAMS_MINUS_ADD == this.type) {
            while ((one + three) <= two || one == 0 || two == 0 || three == 0 || one < two||two>10||three>10) {
                one = rand.nextInt(range);
                two = rand.nextInt(10);
                three = rand.nextInt(10);
            }
            return one + "-" + two + "+" + three;
        } else {
            System.out.println("wrong MathTypeEnum type.[" + this.type + "]");
            return null;
        }
    }


}
